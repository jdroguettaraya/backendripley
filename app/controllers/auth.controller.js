const config = require("../config/auth.config");
const db = require("../models");
const Client = db.client;

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

exports.signup = (req, res) => {
    if (!req.body || !req.body.username || !req.body.email || !req.body.rut || !req.body.password) {
        return res.status(400).send({
            message: "Todos los campos son requeridos."
        });
    }

    const client = new Client({
        username: req.body.username,
        email: req.body.email,
        rut: req.body.rut,
        password: bcrypt.hashSync(req.body.password, 8),
        amount: 0,
        transactions: []
    });

    client.save(err => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }
        res.send({ message: "Cliente creado con exito." });
    });
}

exports.signin = (req, res) => {
    Client.findOne({
        rut: req.body.rut
    })
        .populate("transactions", "-__v")
        .exec((err, client) => {
            if (err) {
                res.status(500).send({ message: err });
                return;
            }
        
            if (!client) {
                return res.status(404).send({ message: "Cliente no encontrado." });
            }

            var passwordIsValid = bcrypt.compareSync(
                req.body.password,
                client.password
            );

            if (!passwordIsValid) {
                return res.status(401).send({
                    accessToken: null,
                    message: "Contraseña no valida."
                });
            }

            var token = jwt.sign({ id: client.id }, config.secret, {
                expiresIn: 900 // 15 minutos
            });

            res.status(200).send({
                id: client._id,
                accessToken: token
            });
        });
}

exports.verifySesion = (req, res) => {
    let token = req.headers["x-access-token"];

  if (!token) {
    return res.status(403).send({ message: "El token es requerido!" });
  }

  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      return res.status(401).send({ message: "No autorizado!" });
    }
    res.send({ message: 'ok'});
  });
}