const db = require("../models");
const Client = db.client;
const Transaction = db.transaction;

exports.getClientData = (req, res) => {
    const id = req.userId;
    
    Client.findById(id).populate("transactions")
        .then(data => {
            if (!data)
                res.status(404).send({ message: "Cliente no encontrado, vuelve a iniciar sesion. " });
            else res.send(data);
        })
        .catch(err => {
            res
                .status(500)
                .send({ message: "Error al buscar al cliente." });
        });
};

exports.getIdByRut = (req, res) => {
    if (!req.query.rut) {
        return res.status(400).send({
            message: "El rut es requerido."
        });
    }
    var condition = {rut: { $eq: req.query.rut }};
    
    Client.find(condition)
        .then(data => {
            if (!data || data.length !== 1)
                res.status(404).send({ message: "Cliente no encontrado" });
            else {
                res.send({
                    id: data[0].id,
                    username: data[0].username
                });
            };
        })
        .catch(err => {
            res
                .status(500)
                .send({ message: "Error al buscar al cliente." });
        });
};

exports.chargeMoney = (req, res) => {
    if (!req.body.amount) {
        return res.status(400).send({
            message: "El monto es requerido"
        });
    }

    if (req.body.amount <= 0) {
        return res.status(400).send({
            message: "El monto a cargar debe ser mayor que 0"
        });
    }
    
    const id = req.params.id;
    const updateObjet = {$inc: {amount: req.body.amount}}
      
    Client.findByIdAndUpdate(id, updateObjet, { useFindAndModify: false })
        .then(data => {
            if (!data) {
                res.status(404).send({
                message: `No se pudo cargar el saldo. Cliente no encontrado.`
                });
            } else res.send({ message: "Salgo cargado." });
        })
        .catch(err => {
            res.status(500).send({
                message: "Error al actualizar el saldo."
            });
        });
};

exports.drawMoney = (req, res) => {
    const id = req.userId;

    if (!req.body.amount) {
        return res.status(400).send({
            message: "El monto es requerido."
        });
    }

    let value;
    if(req.body.amount > 0) {
        value = req.body.amount * -1;
    } else {
        value = req.body.amount;
    }

    Client.findById(id)
        .then(data => {
            if (!data)
                res.status(404).send({ message: "Cliente no encontrado, vuelve a iniciar sesion. " });
            else {
                if(data.amount < (value*-1)) {
                    res.status(400).send({ message: "Monto insuficiente para retirar" });
                } else {
                    const updateObjet = {$inc: {amount: value}}

                    Client.findByIdAndUpdate(id, updateObjet, { useFindAndModify: false })
                        .then(data => {
                            if (!data) {
                                res.status(404).send({
                                message: `No se pudo cargar el saldo. Cliente no encontrado.`
                                });
                            } else res.send({ message: "Retiro efectudo con exito." });
                        })
                        .catch(err => {
                            res.status(500).send({
                                message: "Error al actualizar el saldo."
                            });
                        });
                }
            };
        })
        .catch(err => {
            res
                .status(500)
                .send({ message: "Error al buscar al cliente con id: " + id });
        });
};

exports.pushInfoTransaction = (req, res) => {
    if (!req.body || !req.body.id || !req.body.type || !req.body.amount) {
        return res.status(400).send({
            message: "El id tipo y monto son campos requeridos."
        });
    }
    
    const id = req.body.id;
    const transaction = {
        type: req.body.type,
        amount: req.body.amount,
        detail: req.body.detail || '',
        createdAt: Date.now()
    }

    Transaction.create(transaction).then(transactionObjet => {
        const updateObjet = {$push: {transactions: transactionObjet}}
        console.log(updateObjet);
        
        Client.findByIdAndUpdate(id, updateObjet, { useFindAndModify: false })
            .then(data => {
                if (!data) {
                    res.status(404).send({
                    message: `No se pudo incluir el registro. Cliente no encontrado`
                    });
                } else res.send({ message: "Registro cargado." });
            })
            .catch(err => {
                console.error(err);
                res.status(500).send({
                    message: "Error al incluir el registro."
                });
            });
    });
};