const db = require("../models");
const Client = db.client;

checkDuplicateRut = (req, res, next) => {
  Client.findOne({
    rut: req.body.rut
  }).exec((err, client) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    if (client) {
      res.status(400).send({ message: "Error al crear la cuenta. Solo se permite una cuenta por Rut." });
      return;
    }
  
    next();
  });
};

const verifyRegister = {
    checkDuplicateRut
};

module.exports = verifyRegister;