const { authJwt } = require("../middlewares");
const controller = require("../controllers/account.controller");

module.exports = function(app) {
    app.use(function(req, res, next) {
        res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });
    app.get("/account/getClientData", [authJwt.verifyToken], controller.getClientData);
    app.put("/account/chargeMoney/:id", [authJwt.verifyToken], controller.chargeMoney);
    app.put("/account/drawMoney", [authJwt.verifyToken], controller.drawMoney);
    app.get("/account/getIdByRut", [authJwt.verifyToken], controller.getIdByRut);
    app.post("/account/pushInfoTransaction", [authJwt.verifyToken], controller.pushInfoTransaction);
};