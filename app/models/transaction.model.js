const mongoose = require("mongoose");

const Transaction = mongoose.model(
  "Transaction",
  new mongoose.Schema({
    type: String,
    amount: String,
    detail: String,
    createdAt: Date
  })
);

module.exports = Transaction;