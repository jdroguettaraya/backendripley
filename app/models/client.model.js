const mongoose = require("mongoose");

const Client = mongoose.model(
  "Client",
  new mongoose.Schema({
    username: String,
    rut: String,
    email: String,
    password: String,
    amount: {
      type: Number,
      min: [0, 'Monto minimo'],
    },
    transactions: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Transaction"
      }
    ]
  })
);

module.exports = Client;